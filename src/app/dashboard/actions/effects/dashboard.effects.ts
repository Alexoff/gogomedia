import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from 'rxjs';
import {Action, Store} from '@ngrx/store';
import {State} from '../reducer/dashboard.reducers';
import {map, switchMap} from 'rxjs/operators';
import {DashboardService} from '../../services/dashboard.service';
import {DashboardActionTypes, LoadDashboardSuccessAction} from '../dashboard.action';
import {Dashboard} from '../../models/dashboard.model';


@Injectable()
export class DashboardEffects {

  constructor(private actions: Actions,
              private dashboardService: DashboardService,
              private store: Store<State>) {
  }

  @Effect()
  getDashboard: Observable<Action> = this.actions.pipe(
    ofType(DashboardActionTypes.LoadDashboard),
    switchMap(() => {
      return this.dashboardService.getDashboards().pipe(
        map((dashboards: Dashboard[]) => new LoadDashboardSuccessAction(dashboards))
      );
    })
  );
}

import {Action} from '@ngrx/store';
import {Dashboard} from '../models/dashboard.model';

export enum DashboardActionTypes {
  LoadDashboard = '[Dashboard] Load dashboard action',
  LoadDashboardSuccess = '[Dashboard] Load dashboard success action'
}

export class LoadDashboardAction implements Action {
  public readonly type = DashboardActionTypes.LoadDashboard;
}

export class LoadDashboardSuccessAction implements Action {
  public readonly type = DashboardActionTypes.LoadDashboardSuccess;

  constructor(public dashboards: Dashboard[]) {
  }
}

export type DashboardAction =
  LoadDashboardAction |
  LoadDashboardSuccessAction ;

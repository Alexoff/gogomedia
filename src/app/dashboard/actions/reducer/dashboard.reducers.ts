import {Dashboard} from '../../models/dashboard.model';
import {DashboardAction, DashboardActionTypes} from '../dashboard.action';


export interface State {
  dashboards: Dashboard[];
}

export const initialState: State = {
  dashboards: null,
};

export function dashboardReducer(state = initialState, action: DashboardAction): State {
  switch (action.type) {
    case DashboardActionTypes.LoadDashboardSuccess:
      return {
        ...state,
        dashboards: action.dashboards
      };
    default:
      return state;
  }
}

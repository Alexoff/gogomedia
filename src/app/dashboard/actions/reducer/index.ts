import * as fromDashboardState from './dashboard.reducers';
import {dashboardReducer} from './dashboard.reducers';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface DashboardsState {
  dashboardsList: fromDashboardState.State;
}

export const reducers = {
  dashboardsList: dashboardReducer
};

export const getDashboardsState = createFeatureSelector<DashboardsState>('dashboards');

export const getDashboardsListState = createSelector(
  getDashboardsState,
  (state: DashboardsState) => state.dashboardsList
);

export const getDashboards = createSelector(
  getDashboardsListState,
  (state: fromDashboardState.State) => state.dashboards
);

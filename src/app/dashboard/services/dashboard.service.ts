import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Dashboard} from '../models/dashboard.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getDashboards(): Observable<Dashboard[]> {
    return this.http.get<Dashboard[]>(`${this.baseUrl}/dashboard`).pipe(
      map((dashboards: Dashboard[]) => dashboards.map(dashboard => new Dashboard(dashboard)))
    );
  }
}

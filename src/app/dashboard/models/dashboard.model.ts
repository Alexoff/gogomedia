export class Dashboard {
  public name: string;
  public description: string;

  constructor(obj) {
    this.name = obj.name;
    this.description = obj.description;
  }
}

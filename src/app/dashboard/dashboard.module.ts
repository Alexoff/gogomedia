import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {StoreModule} from '@ngrx/store';
import {reducers} from './actions/reducer';
import {EffectsModule} from '@ngrx/effects';
import {DashboardEffects} from './actions/effects/dashboard.effects';



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    StoreModule.forFeature('dashboards', reducers),
    EffectsModule.forFeature([DashboardEffects]),
  ]
})
export class DashboardModule { }

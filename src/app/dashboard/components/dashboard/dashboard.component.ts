import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../actions/reducer/dashboard.reducers';
import {LoadDashboardAction} from '../../actions/dashboard.action';
import {Dashboard} from '../../models/dashboard.model';
import {Subscription} from 'rxjs';
import * as fromDashbaords from '../../actions/reducer/index';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  public dashboards: Dashboard[];
  private dashboardsSub$: Subscription;

  constructor(private store: Store<State>, private router: Router) {
  }

  ngOnInit(): void {
    this.store.dispatch(new LoadDashboardAction());
    this.dashboardsSub$ = this.store.select(fromDashbaords.getDashboards).subscribe((dashboards: Dashboard[]) => this.dashboards = dashboards);
  }

  ngOnDestroy(): void {
    this.dashboardsSub$.unsubscribe();
  }

  logout(): void {
    localStorage.removeItem('authorization');
    this.router.navigate(['']);
  }

}

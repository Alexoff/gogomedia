import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotAuthGuard} from './auth/guards/not-auth.guard';
import {AuthGuard} from './auth/guards/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'auth', loadChildren: () => import('./auth/auth.module').then(mod => mod.AuthModule), canActivate: [NotAuthGuard]},
  {path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(mod => mod.DashboardModule), canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

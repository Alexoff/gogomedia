import {Injectable} from '@angular/core';
import {CanActivate, Router, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class NotAuthGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token: string = localStorage.getItem('authorization');
    const jwtService = new JwtHelperService();
    if (token && !jwtService.isTokenExpired(token)) {
      this.router.navigate(['dashboard']);
      return false;
    }
    localStorage.removeItem('authorization');
    return true;
  }
}

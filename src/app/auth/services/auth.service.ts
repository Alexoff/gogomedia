import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthModel} from '../models/auth.model';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {User} from '../models/user.model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private router: Router) {
  }

  login(login: AuthModel): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}/auth/login`, login).pipe(
      tap((token: any) => {
        localStorage.setItem('authorization', token.access_token);
        this.router.navigate(['dashboard']);
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(token.access_token);
        return new User(decodedToken);
      })
    );
  }
}

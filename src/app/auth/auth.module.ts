import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthComponent} from './components/auth/auth.component';
import {AuthRoutingModule} from './auth-routing.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './actions/effects/auth.effects';
import {ReactiveFormsModule} from '@angular/forms';
import {reducers} from './actions/reducer';
import {ErrorMessageComponent} from './components/error-message/error-message.component';


@NgModule({
  declarations: [AuthComponent, ErrorMessageComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature('auth', reducers),
    EffectsModule.forFeature([AuthEffects]),
  ]
})
export class AuthModule { }

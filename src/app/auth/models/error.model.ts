export class Error {
  public message: string;
  public status: string;

  constructor(obj) {
    this.message = obj.message;
    this.status = obj.status;
  }
}

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {State} from '../../actions/reducer/auth.reducers';
import {LogInAction} from '../../actions/auth.action';
import * as fromAuth from '../../actions/reducer/index';
import {Subscription} from 'rxjs';
import {Error} from '../../models/error.model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  public authForm: FormGroup;
  public error: Error;

  private errorSub$: Subscription;

  constructor(private store: Store<State>) {
    this.errorSub$ = this.store.select(fromAuth.getError).subscribe((err: Error) => {
      this.error = err;
      setTimeout(() => this.error = null, 10000);
    });
  }

  ngOnInit(): void {
    this.prepareForm();
  }

  prepareForm(): void {
    this.authForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
  }

  onSubmit(): void {
    if (this.authForm.invalid) {
      return;
    }
    this.store.dispatch(new LogInAction(this.authForm.value));
  }
}

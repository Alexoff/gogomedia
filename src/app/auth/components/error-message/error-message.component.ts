import {Component, Input} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss']
})
export class ErrorMessageComponent {
  @Input() control: FormGroup | FormControl;

  get errorMessage(): any {
    for (const key in this.control.errors) {
      if (this.control.errors.hasOwnProperty(key) && this.control.invalid && this.control.touched) {
        return this.getValidationMessage(key, this.control.errors[key]);
      }
    }
    return null;
  }

  private getValidationMessage(validatior: string, validatorValue?: any): any {
    const messages = {
      required: 'This field required',
      email: 'Wrong email'
    };

    return messages[validatior];
  }

}

import {AuthAction, AuthActionTypes} from '../auth.action';
import {User} from '../../models/user.model';
import {Error} from '../../models/error.model';


export interface State {
  user: User;
  error: Error;
}

export const initialState: State = {
  user: null,
  error: null,
};

export function authReducer(state = initialState, action: AuthAction): State {
  switch (action.type) {
    case AuthActionTypes.LogInFailture:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
}

import {Action} from '@ngrx/store';
import {AuthModel} from '../models/auth.model';
import {User} from '../models/user.model';
import {Error} from '../models/error.model';


export enum AuthActionTypes {
  LogIn = '[Auth] Log in action',
  LogInSuccess = '[Auth] Log in success action',
  LogInFailture = '[Auth] Log in failture action'
}

export class LogInAction implements Action {
  public readonly type = AuthActionTypes.LogIn;

  constructor(public auth: AuthModel) {
  }
}

export class LogInSuccessAction implements Action {
  public readonly type = AuthActionTypes.LogInSuccess;

  constructor(public user: User) {
  }
}

export class LogInFailtureAction implements Action {
  public readonly type = AuthActionTypes.LogInFailture;

  constructor(public error: Error) {
  }
}

export type AuthAction =
  LogInAction |
  LogInSuccessAction |
  LogInFailtureAction;
